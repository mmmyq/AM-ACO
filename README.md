#AM-ACO

This directory contains the main code of the proposed methods in the following paper:

Qiang Yang, Wei-Neng Chen, Zhengtao Yu, Tianlong Gu, Yun Li, Huaxiang Zhang, and Jun Zhang. "Adaptive Multimodal Continuous Ant Colony Optimization", IEEE Transactions on Evolutionary Computation, vol. 21, no. 2, pp. 191-205, 2017.

Before compiling and running the code, the following notice should be paid attention to:

1) In the code, the random number generator in "boost" library is utilized. The used version of "boost" is "boost_1_46_1". Thus before compiling this code, please make sure that your computer has already installed this library.

2) The code is implemented on the Ubuntu system. Generally, it can be adapted to any Linux system.

3) The main parameter settings are listed in "Self_Define_Functions.h"; There is no other parameter needed to set. So, after compiling, just directly run the executable file.

4) It is better to compile the code using "makefile". If you compile the code in some IDE, you should make sure the CEC'2013 benchmark set is contained in the project.

 **This code is only for academic use and if you use this code, please cite the following paper:** 

Qiang Yang, Wei-Neng Chen, Zhengtao Yu, Tianlong Gu, Yun Li, Huaxiang Zhang, and Jun Zhang. "Adaptive Multimodal Continuous Ant Colony Optimization", IEEE Transactions on Evolutionary Computation, vol. 21, no. 2, pp. 191-205, 2017.

if you have any question, please contact Prof. Wei-Neng Chen(cwnraul634@aliyun.com) or Dr. Qiang Yang (mmmyq@126.com)

If you cannot understand Chinese, you can click the "English" button in the right bottom of this page (简 体 / 繁 體 / English) to transform this page into English Version.