#include "./CEC2013/cec2013.h"
#include "Self_Define_Functions.h"
#include <sys/time.h>
#include <cstdio>
#include <unistd.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <iomanip>
#include <string>
#include <boost/random.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/cauchy_distribution.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_real.hpp>


using namespace std;

int main(int argc, char *argv[])
{

    int funToRun[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};  //function set

    int funNum = 20; //the number of functions to run

    int population_size_set[] = {80,80,80,80,80,100,300,300,300,100,200,200,200,200,200,200,200,200,200,200}; //the population size setting for each function; this is just directly borrowed from existing literature for fair comparison

    int neighbor_size_set[] = {2,4,8,12,16,20};//the niche size set

    int neighbor_size_set_size = 6; // the niche size set size


    int i,j,k,fun,run;
    CEC2013 *pFunc = NULL;
    int population_size;
    int dim;
    int Fes;
    int MAX_FES;
    int temp_global_optima_size;
    int global_optima_num;
    int N_nearest;//neighbour size

    int record_count=0;

    int **runs_global_num = new int*[epsilon_set_size];//to store the  number of found global optima at each accuracy level
    int **runs_used_fitness = new int*[epsilon_set_size];//to store the used number of fitness evaluations at each accuracy level when all global optima are found

    int record_size = 10;//to record the optimization results every 1/10 fitness evaluations

    int *record_fes = new int[record_size];//the corresponding fitness evaluations to record the results

    int ***runs_global_num_vs_fes = new int**[epsilon_set_size];//to record the found global optima at the associated recorded fitness evaluations at each accuracy level

    bool *epsilon_flag = new bool[epsilon_set_size];

    for( i = 0; i < epsilon_set_size; ++i )
    {
        runs_global_num[i] = new int [timesOfRun];
        runs_used_fitness[i] = new int[timesOfRun];
        runs_global_num_vs_fes[i] = new int*[timesOfRun];

        for( j = 0; j < timesOfRun; ++j )
        {
            runs_global_num_vs_fes[i][j] =  new int[record_size];
        }
    }

    int species_num;//the number of divided species
    int last_species_size;//the number of individuals in the last species

    //the parameter setting for local search engine
    double local_std_value = 1e-4;
    int local_sample_num = 5;



    for ( fun =0; fun <funNum; fun ++)
    {
        cout<<"Function "<<funToRun[fun]<<" Begined!"<<endl;

        pFunc = new CEC2013(funToRun[fun]); //obtain the function handle

        dim = pFunc->get_dimension();//obtain the dimension size

        global_optima_num = pFunc->get_no_goptima();//obtain the true number of global optima

        MAX_FES = pFunc->get_maxfes();//obtain the allowed maximum fitness evaluations


        double *LBound = new double[dim];//the lower bound of each dimension
        double *UBound = new double[dim];//the upper bound of each dimension

        for( i = 0; i < dim; ++i )
        {
            LBound[i] = pFunc->get_lbound(i);
            UBound[i] = pFunc->get_ubound(i);

        }


        for( i = 0; i < record_size; ++i )
        {
            record_fes[i] = (int)((double(i+1))/record_size * MAX_FES);
        }

        population_size = population_size_set[funToRun[fun]-1];//the population size

        //allocation the resources
        double **population = new double*[population_size];
        double **dist = new double*[population_size];
        double **child = new double*[population_size];
        int **species = new int*[population_size];
        for( i = 0; i < population_size; ++i )
        {
            population[i] = new double[dim];
            child[i] = new double[dim];
            dist[i] = new double[population_size];
            species[i] = new int[ population_size];
        }


        double *population_results = new double [population_size];
        double *child_results = new double[population_size];


        double *reference_point = new double[dim];//used by the niche method:clustering for crowding
        int *seed_location = new int [population_size];
        double *species_local_prob = new double [population_size];


        Fes = 0;

        boost::mt19937 generator(time(0)*rand());
        boost::uniform_int<> uniform_int_generator( 0, neighbor_size_set_size-1 );
        boost::variate_generator< boost::mt19937&, boost::uniform_int<> > random_int_num( generator, uniform_int_generator);

        for (run = 0; run < timesOfRun; run++)
        {
            cout<<"Running the "<<run<<"th times"<<endl;

            for( i = 0; i < epsilon_set_size; ++i )
            {
                epsilon_flag[i] = false;
            }

            Fes = 0;
            record_count=0;

            // initialize the population
            for( j =0; j < dim; ++j )
            {
                boost::uniform_real<> uniform_real_generate_x( LBound[j], UBound[j] );
                boost::variate_generator< boost::mt19937&, boost::uniform_real<> > random_real_num_x( generator, uniform_real_generate_x );

                for( i =0 ; i < population_size; ++i )
                {
                    population[i][j] = random_real_num_x();
                }
            }

            //compute the fitness of the population
            Popupation_Fitness( population, population_size, Fes, population_results, pFunc );

            //compute the distance between individuals
            Dist_All_to_All( population, dist, population_size, dim );


            while( Fes < MAX_FES )
            {
                //compute the number of global optima found
                for( i = 0; i < epsilon_set_size; ++i )
                {
                    if( !epsilon_flag[i] )
                    {
                        //compute the found global optima at current generation
                        temp_global_optima_size = Compute_Global_Optima_Found( population,  population_results, population_size, dim, epsilon_set[i], pFunc );
                        runs_global_num[i][run] = temp_global_optima_size;
                        runs_used_fitness[i][run] = Fes;
                        if( temp_global_optima_size == global_optima_num )//whether the algorithm has found all the global optima
                        {
                            epsilon_flag[i] = true;
                        }
                    }
                }

                if( Fes >= record_fes[record_count] )//record the optimization results
                {
                    for( i = 0; i < epsilon_set_size; ++i )
                    {
                        runs_global_num_vs_fes[i][run][record_count] = runs_global_num[i][run];
                    }

                    record_count++;
                }

                N_nearest = neighbor_size_set[ random_int_num() ];//randomly select a niche size from the niche size pool
                species_num = population_size / N_nearest;//compute the number of species

                if( population_size % N_nearest == 0 )
                {
                    last_species_size = 0;
                }
                else//if the remainder is not 0, then put all the remained individuals as a new species
                {
                    species_num++;
                    last_species_size = population_size % N_nearest;
                }


                //randomly generate a reference point
                Generate_Reference_Point( reference_point, LBound, UBound, dim );

                //divide the population into niches
                Species( reference_point, population, species, dist, species_num, N_nearest, last_species_size, population_size, dim );

                //obtain the best individual in each niche
                Get_Seed_Location( seed_location, population_results, species, species_num, N_nearest, last_species_size );

                //evolve each niche
                Evolve_At_Individual3( species, population, population_results, child, LBound, UBound, seed_location, population_size, dim, species_num, N_nearest, last_species_size );

                //compute the fitness of the offspring
                Popupation_Fitness( child, population_size, Fes, child_results, pFunc );

                //select individuals for the next generation
                Selection( population, child, dist, population_results, child_results, population_size, dim );

                //obtain the best individual in each niche
                Get_Seed_Location( seed_location, population_results, species, species_num, N_nearest, last_species_size );

                //compute the probability of each niche to conduct local search
                Seed_Local_Prob( species_local_prob, seed_location, population_results, species_num );

                //conduct local search
                Local_Evolve( population, population_results, LBound, UBound, species_local_prob, seed_location, dim, species_num, local_std_value, Fes, pFunc, local_sample_num );

                //update the distance between individuals
                Update_Dist_Seed( dist, seed_location, population, population_size, species_num, dim );



            }

            //compute the number of global optima found at current generation
            for( i = 0; i < epsilon_set_size; ++i )
            {
                if( !epsilon_flag[i] )
                {
                    temp_global_optima_size = Compute_Global_Optima_Found( population, population_results, population_size, dim, epsilon_set[i], pFunc );

                    runs_global_num[i][run] = temp_global_optima_size;
                    runs_used_fitness[i][run] = MAX_FES;

                }
            }

            for( i = 0; i < epsilon_set_size; ++i )
            {
                runs_global_num_vs_fes[i][run][record_count] = runs_global_num[i][run];
            }


        }

        //write the results to files
        char fun_name[10];
        char epsilon_counter[10];
        snprintf(fun_name,10,"%d",funToRun[fun]);

        string *Epsilon_Files = new string[epsilon_set_size];
        ofstream *out_Epsilon = new ofstream[epsilon_set_size];

        for( i = 0; i < epsilon_set_size; ++i )
        {
            snprintf(epsilon_counter,10,"%d",i+1);
            Epsilon_Files[i] = "./Results/"+string(epsilon_counter)+"/"+"Final_Optima_Num_And_FES_Epsilon_"+string(epsilon_counter)+"_Fun_"+ string(fun_name)+".txt";
        }



        for( i = 0; i < epsilon_set_size; ++i )
        {
            out_Epsilon[i].open( Epsilon_Files[i].c_str() );
        }


        for( i = 0; i < epsilon_set_size; ++i )
        {
            for( j = 0; j < timesOfRun; ++j )
            {
                out_Epsilon[i]<<runs_global_num[i][j] <<"\t"<<runs_used_fitness[i][j]<<endl;
            }
        }


        for( i = 0; i < epsilon_set_size; ++i )
        {
            out_Epsilon[i].close();
        }

        for( i = 0; i < epsilon_set_size; ++i )
        {
            snprintf(epsilon_counter,10,"%d",i+1);
            Epsilon_Files[i] = "./Results/"+string(epsilon_counter)+"/"+"Optima_Num_VS_FES_Epsilon_"+string(epsilon_counter)+"_Fun_"+ string(fun_name)+".txt";
        }


        for( i = 0; i < epsilon_set_size; ++i )
        {
            out_Epsilon[i].open( Epsilon_Files[i].c_str() );
        }

        for( i = 0; i < epsilon_set_size; ++i )
        {
            for( j = 0; j < timesOfRun; ++j )
            {
                for( k = 0; k < record_size; ++k )
                    out_Epsilon[i]<<runs_global_num_vs_fes[i][j][k] <<"\t";
                out_Epsilon[i]<<endl;
            }
        }

        for( i = 0; i < epsilon_set_size; ++i )
        {
            out_Epsilon[i].close();
        }



        cout<<"Function "<<funToRun[fun]<<" Finished!"<<endl;


        //release the resources
        for( i =0; i < population_size; ++i )
        {
            delete []population[i];
            delete []dist[i];
            delete []child[i];
            delete []species[i];
        }
        delete []population;
        delete []dist;
        delete []child;
        delete []species;

        delete []population_results;
        delete []child_results;
        delete []reference_point;
        delete []LBound;
        delete []UBound;
        delete []seed_location;
        delete []species_local_prob;

        delete []Epsilon_Files;
        delete []out_Epsilon;

	}


    for( i = 0; i < epsilon_set_size; ++i )
    {
        delete []runs_global_num[i];
        delete []runs_used_fitness[i];
        for( j = 0; j < timesOfRun; ++j )
        {
            delete []runs_global_num_vs_fes[i][j];
        }
        delete []runs_global_num_vs_fes[i];

    }

    delete []runs_global_num;
    delete []runs_used_fitness;
    delete []epsilon_flag;
    delete []runs_global_num_vs_fes;

    delete []record_fes;


    delete pFunc;

    return 0;
}



