#ifndef SELF_DEFINE_FUNCTIONS_H_INCLUDED
#define SELF_DEFINE_FUNCTIONS_H_INCLUDED


#include "./CEC2013/cec2013.h"
#include <math.h>
#include <boost/random.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/cauchy_distribution.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_real.hpp>
#include <vector>


using namespace std;

const int timesOfRun = 51;//the number of independent runs


const double epsilon_set[] = {1e-1,1e-2,1e-3,1e-4,1e-5};//the accuracy level set
const int epsilon_set_size = 5;


struct NewType
{
    double data;
    int id;
};

//compare two struct variables
bool Compare_NewType( NewType data1, NewType data2 );

//compute the fitness of an individual
double Fitness ( double *particle, int &Fes, CEC2013 *pFunc );

//compute the fitness of a population
void Popupation_Fitness( double **population, int population_size, int &Fes, double *result, CEC2013 *pFunc );

//compute the Euclidean distance between two vectors
double Distance( double *vector1, double *vector2, int dim );

//compute the Euclidean distance between two vectors
double Distance( double *vector1, vector<double> vector2, int dim );

//obtain the candidate global optima of a population
void Get_Seeds( double **population, double *population_results, int population_size, int dim, vector<double> &seed_fitness, double radius );

//obtain the number of global optima
int How_Many_Global_Optima( vector<double> seed_fitness, double epsilon, CEC2013 *pFunc );

//obtain the number of found global optima in a population
int Compute_Global_Optima_Found( double **population, double *population_results, int population_size, int dim, double epsilon, CEC2013 *pFunc );

//find the closest individual of the child
int Close_Particle( double *child, double **population, int population_size, int dim );

//find the closest individual based on the computed distance
int Close_Particle( double *dist,  int population_size );

//compute the distance between an individual and each individual in the  population
void Dist_Individual_to_All( double *dist, double *individual, double **population, int population_size, int dim );

//compute the distance between any individuals in the  population
void Dist_All_to_All( double **population, double **dist, int population_size, int dim );

//update the distance matrix based on a distance vector
void Update_Dist( double **dist, double *temp_dist, int closet_individual, int population_size );

//sort the population based on the computed distance
void Sort_Population_Dist( double *dist, int *sorted_index, int population_size );

//sort the population based on the computed fitness
void Sort_Population_Fitness( double *population_results, int *sorted_index, int population_size );

//obtain the ranking of each individual based on the computed fitness
void Ranking( double *population_results, int *ranking, int population_size, double &fitness_gap );

//obtain the M nearest individuals of an individual based on the computed distance
void Get_Nearest_M_Individuals( double *dist, bool *flag, int *Nearest_M, int population_size, int n_nearest );

//divide the population into niches (crowding)
void Species( double *reference_point, double **population, int **species, double **dist,
              int species_num, int N_nearest, int last_species_size, int population_size, int dim );

//obtain the seed of each niche
void Get_Seed_Location( int *seed_location, double *population_results, int **species, int species_num, int speices_size, int last_species_size );

//compute the probability of each niche to conduct local search
void Seed_Local_Prob( double *seed_local_prob, int *seed_location, double *population_results, int species_num );

//local search conducted on the seed of each niche
void Local_Species_Evolve(  double **population, double *population_results, double *LBound, double *UBound, int seed_location,
                            int dim,  double local_std_value , int &Fes, CEC2013 *pFun, int sample_num );

//local search conducted on the seed of each niche
void Local_Species_Evolve(  double **population, double *population_results, double *LBound, double *UBound, int seed_location,
                            int dim,  double local_std_value , int &Fes, CEC2013 *pFun, int sample_num, int &local_search_fail );

//local search conducted on the seed of each niche
void Local_Evolve(  double **population, double *population_results, double *LBound, double *UBound, double *seed_local_prob, int *seed_location,
                    int dim, int species_num,  double local_std_value, int &Fes, CEC2013 *pFun, int sample_num );

//compute the weight of each ant in each species
void Weight_in_Species( double *weight, int *species, double*population_results, int species_size, double population_fitness_gap );

//compute the selection probability of each ant in each species
void Probability_in_Species( double *probability,  double *weight, int species_size );


//compute the variance
double Cal_Variance( int *species, double **population, int dim_index, double dim_value, double epsilon_norm, int species_size );

//compute the variance
void Cal_Variance( double *variance, int *species, double **population, double *selected_individual,  double epsilon_norm, int species_size, int dim );

//randomly select an individual
int Selected_Individual( double *probability, int species_size );

//evolve each species
void Species_Evolve_At_Individual3( int *species, double **population, double*population_results, double **child, double *LBound, double *UBound, int seed_location,
                                    int dim, int species_size, double population_fitness_gap );


//evolve the population
void Evolve_At_Individual3( int **species, double **population, double *population_results, double **child, double *LBound, double *UBound, int *seed_location,
                            int population_size, int dim, int species_num, int n_nearest, int last_species_size );

//get f_max-f_min namely the best fitness - the worst fitness
double Get_Population_Fitness_Gap( double *population_results, int population_size );

//select individuals for next generation
void Selection( double **population, double **child, double **dist, double *population_results, double *child_results,
                int population_size, int dim );

//randomly generate a reference point
void Generate_Reference_Point( double *reference_point, double *LBound, double *UBound, int dim );

//update the distance  between individuals
void Update_Dist_Seed( double **dist, int *seed_location, double **population, int population_size, int seed_num, int dim );


#endif // SELF_DEFINE_FUNCTIONS_H_INCLUDED
